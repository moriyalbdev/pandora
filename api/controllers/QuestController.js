"use strict"
const _ = require("lodash")
const fs = require("fs")
const ejs = require("ejs")

let Controller = module.exports
let Quests = require("../source/Quests")

Controller.initQuests = async function(){    
    let result = await Quest.find({}) 
    for (let r of result){
        let [qtype,index] = r.qid.split("_")
        Quests.maxIndex += 1
        Quests.maxIndex = Math.max(index, Quests.maxIndex)
        Quests.quests[qtype].push({
            "type":"quest",
            "value":r.quest,
			"qid":r.qid
        })
        Quests.answers[r.qid] = r.answer
    }
    for (let key in Quests.quests){
        Quests.quests[key].sort((f, s)=>{
            let [,fi] = f.qid.split("_")
            let [,si] = s.qid.split("_")
            return fi - si
        })
    }
    //console.log("Check Quest -> ", Quests)
}

Controller.saveQuest = async function(req, res){
    console.log("saveQuest ", JSON.stringify(req.params.all()))
    let {qid, quest, answer, isnew} = req.params.all()
    let qtype
    if (isnew) {
        qtype = qid
        Quests.maxIndex += 1
        qid = qtype + "_" + Quests.maxIndex
    }else{
        qtype = qid.split("_")[0]
    }
    if (!_.has(Quests.quests, qtype)){
        res.json({
            result:"fail"
        })
    }
        
    
    if (isnew){
        await Quest.create({
            qid:qid,
            quest:quest,
            answer:answer
        })
        Quests.quests[qtype].push({
            "type":"quest",
            "value":quest,
            "qid":qid
        })
    }else{
        await Quest.update({qid:qid}, {
            qid:qid,
            quest:quest,
            answer:answer
        })
        for (let qv of Quests.quests[qtype]){
            if (qv.qid == qid){
                qv.value = quest
            }
        }
    }
    Quests.answers[qid] = answer
    
    res.json({
        result:"ok",
        qid:qid
    })
}

Controller.fetchQuest = async function(req, res){
    console.log("fetchQuest ", JSON.stringify(req.params.all()))
    let {qtype} = req.params.all()
    return await Controller.fetchQuestFromPage(qtype, res)
}

Controller.fetchQuestFromPage = async function(qtype, res){
    res.json({
		result:"ok",
		value:{
			content:Quests.quests[qtype]
		}        
    })
}

Controller.getQuestIds = async function(req, res){
    let {qtype} = req.params.all()
    let ids = _.map(Quests.quests[qtype], 'qid')
    res.json({
		result:"ok",
        ids:ids
    })
}

Controller.getQuestById = async function(req, res){
    let {qid} = req.params.all()
    let [qtype,] = qid.split("_")    
    let qs = Quests.quests[qtype]
    let index = _.findIndex(qs, (v)=>{
        return v.qid == qid
    })
    if (index == -1){
        res.send("")
        return
    }
    let result = {
        qid:qid,
        quest:qs[index].value,
        answer:Quests.answers[qid]
    }
    res.json({
		result:"ok",
		value:result
	})     
}

Controller.deleteQuest = async function(req, res){
    let {qid} = req.params.all()
    console.log("try delete quest -> ", qid)
    let [qtype,] = qid.split("_")
    if (!_.has(Quests.quests, qtype)){
        res.json({
            result:"fail"
        })
    }
    await Quest.destroy({
        qid:qid
    })
    _.remove(Quests.quests[qtype], (v)=>{
        return v.qid == qid
    })
    res.json({
        result:"ok"
    })
}

Controller.fetchAnswer = async function(req, res){
    console.log("fetchAnswer ", JSON.stringify(req.params.all()))
    let {qid, pwd} = req.params.all()
    if (pwd != "1212"){
        res.json({
            result:"fail"
        })
    }else{
        res.json({
            result:"ok",
            ans:Quests.answers[qid]
        })
    }
}