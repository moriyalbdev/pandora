"use strict"
let Controller = module.exports
const fs = require("fs")
const ejs = require("ejs")
const QuestController = require("./QuestController")

const simplepwd = [
		"",
		"da39a3ee5e6b4b0d3255bfef95601890afd80709",
		"7c4a8d09ca3762af61e59520943dc26494f8941b"
	]

Controller.home = function(req, res) {
	req.session.lasturl = req.url
	// if (!req.session.uid){
	// 	res.redirect("/source/login")
	// 	return
	// }
    res.view({
        "title" : "Pandora Source Home(@jjgame)"
    })
}

Controller.login = async function(req, res){
	if (!!req.session.uid){
		res.redirect("/source/home")
		return
	}
	res.view({})
}

Controller.onLogin = async function(req, res){
	let {name, pwd} = req.params.all()
	let userCnt = await User.count({})
	let user = await User.find({name:name})
	
	if (user.length == 0){
		let defUserCfg = require("../../datums/users")
		//console.log("check users -> ", defUserCfg)
		if (name in defUserCfg){			
			user = {
				name:name,
				pwd:"",
				uid: userCnt + 10001,
				privilege:defUserCfg[name],
				nick:"",
				exp:0
			}
			await User.create(user)
			res.json({
				result:"pwdchange"
			})
		}else{
			res.json({
				result:"invaliduser"
			})
		}
	}else{
		user = user[0]
		if (simplepwd.indexOf(user.pwd) >= 0){
			res.json({
				result:"pwdchange"
			})
		}else if (user.pwd != pwd){
			res.json({
				result:"invalidpwd"
			})
		}else{
			req.session.uid = user.uid
			res.json({
				result:"ok",
				url:req.session.lasturl
			})
		}
	}
}

Controller.onChange = async function(req, res){
	let {name, pwdnew} = req.params.all()
	let user = await User.find({name:name})
	if (user.length == 0 || pwdnew == ""){
		res.json({
			result:"fail"
		})
		return
	}
	user = user[0]
	await User.update({uid:user.uid},{pwd:pwdnew})
	req.session.uid = user.uid
	res.json({
		result:"ok",
		url:req.session.lasturl
	})
}

Controller.fetchUserInfo = async function(req, res){
	let {uid} = req.params.all()
	if (uid == ""){
		uid = req.session.uid
	}
	let user = await User.find({uid:uid})
	res.json(user[0])
}

Controller.saveUserInfo = async function(req, res){
	let {pwd, nick} = req.params.all()
	console.log("saveUserInfo ", pwd, nick)
	let uid = req.session.uid
	let update = {}
	if (simplepwd.indexOf(pwd) == -1){
		update.pwd = pwd
	}
	if (nick != ""){
		update.nick = nick
	}
	await User.update({uid:uid}, update)
	res.json({
		result :"ok"
	})
}


Controller.fetchNavData = async function(req, res){
    res.json({
		result:"ok",
		value:require("../../datums/nav")
	})
}

Controller.fetchPageData = async function(req, res){
    let tag = req.params.all().ptag
    //console.log("fetchPageData ", JSON.stringify(req.params.all()))
    if (tag.indexOf("devs.quest") >= 0){
        let [,,qtype] = tag.split(".")
        return await QuestController.fetchQuestFromPage(qtype, res)        
    }

    tag = tag.replace(/\./g, "/")
    let reqpage = function(tag){
        try{
            return require("../../datums/pages/" + tag)
        }catch(e){         
            let dot = tag.lastIndexOf("/")
            return dot == -1 ? reqpage("base") : reqpage(tag.substr(0, dot))
        }        
    }
    let page = reqpage(tag)
    if(!page){
        res.send("")
        return
    }
    let url    
    for (let v of page.content){
        if (v.type=="html"){            
            try{                
                if (v.ejs == "main"){
                    url = "datums/contents/"+tag+".ejs"
                }else{
                    url = "datums/contents/"+tag+"/"+v.ejs+".ejs"
                }                
                let html = fs.readFileSync(url, {encoding:'utf-8'})
                v.value = ejs.render(html)
            }catch(e){
                url = "views/501.ejs"
                let html = fs.readFileSync(url, {encoding:'utf-8'})
                v.value = ejs.render(html)
            }                        
        }
    }
    //console.log(" check ", tag, JSON.stringify(page))
    res.json({
		result:"ok",
		value:page
	})
}
