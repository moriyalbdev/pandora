"use strict"
let Document = module.exports;

Document.attributes = {
	name :{
		type: 'string',
		unique: true,
		required: true
	},
    content: {
		type: 'string',      
		required: true
    }
  }