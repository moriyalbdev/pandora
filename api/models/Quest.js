"use strict"
let Quests = module.exports;

Quests.attributes = {
		qid :{
			type: 'string',
			unique: true,
			required: true
		},
    quest: {
			type: 'string'
    },
    answer: {
			type: 'string'
    }
  }