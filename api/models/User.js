"use strict"
let User = module.exports;

User.attributes = {
	uid :{
		type: "int",
		unique: true,
		required: true
	},
	name:{
		type: "string",
		unique: true,
		required: true
	},
	pwd:{
		type: "string"
	},
	nick:{
		type: "string"
	},
	exp:{
		type: "int"
	},
	privilege:{
		type: "int"
	}
  }