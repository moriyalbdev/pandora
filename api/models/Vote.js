"use strict"
let Vote = module.exports;

Vote.attributes = {
	uid :{
		type: "int",
		unique: true,
		required: true
	},
	title:{
		type: "string",
		unique: true,
		required: true
	},
	desc:{
		type: "string"
	},
	items:{
		type: "array"
	},
	owner:{
		type: "int"
	}
  } 