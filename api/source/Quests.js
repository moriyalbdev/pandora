"use strict"


let Quests = function(){
    this.maxIndex = 0
    this.quests = {
        "newbie":[],
        "client":[],
        "server":[],
        "logic":[],
        "language":[],
        "other":[]
    }
    this.answers = {}
}

module.exports = new Quests()