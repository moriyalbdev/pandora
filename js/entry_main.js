import main from "./main.js"
import $ from "jquery"

$(document).ready(function(){
	main.start()
})

window.redirectto = main.fetchPage.bind(main)
window.fetchquest = main.fetchQuest.bind(main)
window.goback = main.goBack.bind(main)
window.onbeforeunload = function(){
    //$('#myModal').modal({})
}
window.onpopstate = function () {
	//$('#myModal').modal({})
	main.goBack()
}