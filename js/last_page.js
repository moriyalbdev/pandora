import $ from 'jquery'
import _ from 'lodash'

export default class LastPage{
	constructor(){
		this.lastPages = new Array();
		this.baseUrl = ""
		this.fromBack = false
		this.pageUrl = ""
		this.init()
	}

	init(){
		let url = window.location.href
		let tags = url.lastIndexOf("/")
		this.pageUrl = url.substr(tags + 1, url.length)
		if (this.pageUrl == "home"){
			this.baseUrl = url
			this.pageUrl = ""
		}else{
			this.baseUrl = url.substr(0, tags)
		}
	}

	curPage(){
		return this.pageUrl
	}

	setPage(page){
		if (!this.fromBack){
			_.remove(this.lastPages, (v)=>{return v==page})
			history.pushState("", document.title, this.baseUrl+"/"+page)
			this.lastPages.push(page)
		}	
		this.fromBack = false		
	}

	doBack(){
		this.lastPages.pop() //current
		this.fromBack = true
		let url = this.lastPages.pop()
		if (!!url){
			window.redirectto(url)
			//setScroll(window.pagescroll[url] || 0)
		}		
	}

	getScroll(){
		return $("body").scrollTop()
	}

	setScroll(){
		$("body").scrollTop(0)
	}
}