import comp_nav from "../vue/nav.vue"
import comp_nav_item from "../vue/nav_item.vue"
import comp_user_nav from "../vue/user_nav.vue"
import comp_user from "../vue/user.vue"
import comp_quest from "../vue/quest.vue"
import comp_page from "../vue/page.vue"
import comp_modal_dlg from "../vue/modal_dlg.vue"

import Vue from "vue"
import VueResource from "vue-resource"
Vue.use(VueResource)
import $ from 'jquery'
window.$ = $
window.jQuery = $ //global for bootstrap

import _ from "lodash"
import "google-code-prettify/bin/prettify.min"
import "bootstrap"

import LastPage from "./last_page"

class Main {
	constructor(){
		this.navVue = new Vue(comp_nav)
		this.modalDlg = new Vue(comp_modal_dlg)
		this.pageVue = null

		this.lastPage = new LastPage()
	}

	registerComps(){
		Vue.component('nav-item', comp_nav_item)
	}

	start(){
		this.registerComps()
		Vue.http.get("/source/ajax/nav").then(resp=>{
			if (resp.body.result == "ok"){
				this.onLoadNav(resp.body.value)
			}		
		})	
		Vue.http.get("/source/ajax/user?uid=").then(resp=>{
			this.onLoadUser(resp.body)
		})	
	}

	onLoadNav(nav){
		let defPage = this.lastPage.curPage()
		for (let parent of nav.parent){
			let rootItem = nav.root[parent]
			Vue.set(this.navVue.items, parent, {
				name:rootItem.name,
				active:false,
				page:parent
			})
			if (defPage == "" && !!rootItem.default){
				defPage = parent
			}
			for (let child of rootItem.children){
				let subItem = nav.sub[child]
				Vue.set(this.navVue.items, child, {
					parent:parent,
					name:subItem.name,
					sub:true,
					page:parent+"."+child
				})
			}		
		}	
		if (defPage != ""){
			this.fetchPage(defPage)
		}
		//console.log("check -> ",JSON.stringify(navVue.items))
	}

	
	onLoadUser(user){		
		this.userVue = new Vue(comp_user_nav)
		this.userVue.uid = user.uid
		this.userVue.name = user.name
		this.userVue.nick = user.nick
		this.userVue.privilege = user.privilege
	}

	onLoadUserInfo(){
		new Vue(comp_user)
	}

	onLoadAddQuest(){
		new Vue(comp_quest)
	}

	onLoadPage(page){
		//console.log("onLoadPage -> ", page, pageVue)
		if (!this.pageVue){			
			this.pageVue = new Vue(comp_page)
		}

		this.pageVue.items = []
		for (let index in page.content){
			let item = page.content[index]
			Vue.set(this.pageVue.items, index, {
				type:"page_" + item.type,
				value:item.value || item.href || item.src,
				href:item.href,
				codetype:"lang_"+item.codeType,
				page:item.page,
				qid:item.qid
			})
		}	
		this.lastPage.setScroll()
	}


	goBack(){
		this.lastPage.doBack()
	}

	
	fetchPage(page){
		Vue.http.get("/source/ajax/page/" + page).then(resp=>{
			if (resp.body.result == "ok"){
				this.onLoadPage(resp.body.value)
				this.lastPage.setPage(page)						
			}
		})
	}

	fetchQuest(quest){
		let page = "devs.quest."+quest
		Vue.http.get("/source/ajax/quest/" + quest).then(resp=>{
			if (resp.body.result == "ok"){
				this.onLoadPage(resp.body.value)
				this.lastPage.setPage(page)	
			}
		})
	}
}

let main = new Main()
export default main